<?php

namespace webwizardry\widebug;

/**
 * Use this trait to plug debug console in other classes of any application
 * 
 * @example use \webwizardry\widebug\DebugableObject; 
 *
 * @package widebug
 * @author Alexey Volkov <a.a.volkov@outlook.com>
 */

trait DebuggedObject 
{
    //put your code here
}

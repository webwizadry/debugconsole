<?php
namespace webwizardry\widebug;
/**
 * Web Wizardry standalone console for debugging and logging web applications
 *
 * @package widebug
 * @author Alexey Volkov <a.a.volkov@outlook.com>
 */
class Debugger 
{
    /**
     * The levels of logging for a decision on the withdrawal of 
     * a messages
     */
    const
        LOG_SILENT  = 0,
        LOG_ERROR   = 10,
        LOG_WARNING = 20,
        LOG_QUERY   = 30,
        LOG_MESSAGE = 40,
        LOG_ALL     = 100; 
            
    /**
     * Debugger output method
     */
    const  
        OUTPUT_CONSOLE = 0,
        OUTPUT_FILE = 1;

    private $_logLevel = self::LOG_SILENT;
    private $_output   = self::OUTPUT_CONSOLE;
    
    private $_controlCSS = null;
    
    private $_logFile = null;
   
    private static $_log = null;
    
    
    /**
     * Sets up loggging level value
     * 
     * @param type $level
     */
    public function setLogLevel($level)
    {
        $this->_logLevel = $level;
    }
    
    /**
     * Allows to replace core debugger css file with your own one
     * 
     * @param string $cssfile
     */
    public function setControlCSS($cssfile)
    {
        $tihs->_controlCSS = $cssfile;
    }
    
    /**
     * Sets up filename to write log and allows to disable
     * console output
     * 
     * @param string $pathTo
     * @param boolean $swithOutput
     */
    public function useLogFile($pathTo, $swithOutput = false)
    {
        $this->_logFile = $pathTo;
        if($switchOutput){
            $this->_output = self::OUTPUT_FILE;
        }
    }
    
    /**
     * Use Debugger as singletone instane only or use trat DebugableObject
     * 
     * @var type 
     */
    private static $_instance = null;
    
    private function __construct()
    {
        // there is nothing to do here
    }
    
    public static function getInstance()
    {
        if (null === self::$_instance) self::$_instance = new Debugger;
        return self::$_instance;
    }
}
